# README - Parpadeo de LED con Interrupciones en Arduino Uno

Este archivo README proporciona una breve introducción y explicación del código que se encuentra a continuación. El código permite que el LED integrado en una placa Arduino Uno parpadee aproximadamente una vez por segundo utilizando interrupciones de tiempo.

## Descripción del Código

El siguiente código de Arduino se utiliza para hacer parpadear el LED incorporado en una placa Arduino Uno a una frecuencia de aproximadamente 1 segundo. La característica principal de este código es el uso de interrupciones de tiempo para controlar el parpadeo del LED. En lugar de utilizar las bibliotecas estándar de Arduino, se modifican los registros del timer interno de Arduino (Timer2) para generar una interrupción periódica.

```cpp
volatile unsigned int cuenta = 0; // Contabiliza las veces que se llama a ISR
bool ESTADO = false; // Almacena el estado actual del LED
```

- `cuenta` es una variable que lleva un registro del número de veces que se llama a la función de interrupción (ISR).
- `ESTADO` es una variable booleana que almacena el estado actual del LED.

```cpp
void setup() {
  pinMode(13, OUTPUT);
  SREG = (SREG & 0b01111111); // Deshabilitar interrupciones
  TIMSK2 = TIMSK2 | 0b00000001; // Habilita la interrupción por desbordamiento
  TCCR2B = 0b00000111; // Configura la preescala para que FT2 sea de 7812.5Hz
  SREG = (SREG & 0b01111111) | 0b10000000; // Habilitar y deshabilitar interrupciones
}
```

- `setup()` se encarga de la configuración inicial del programa. Establece el pin 13 (donde se encuentra el LED incorporado) como una salida. Luego, configura los registros para habilitar la interrupción de desbordamiento del Timer2 y ajusta la preescala para lograr una frecuencia de desbordamiento de aproximadamente 7812.5 Hz.

```cpp
void loop() {
  // Aquí estaría el código del flujo normal de ejecución
}
```

- `loop()` es el bucle principal del programa donde normalmente se ejecutarían otras tareas. En este caso, no se realiza ninguna acción específica en el bucle principal.

```cpp
ISR(TIMER2_OVF_vect) {
  cuenta++;
  if (cuenta > 29) { // Cada 30 llamadas a ISR cambia el estado del LED
    digitalWrite(13, ESTADO);
    ESTADO = !ESTADO;
    cuenta = 0;
  }
}
```

- `ISR(TIMER2_OVF_vect)` es la función de interrupción que se llama cada vez que se produce un desbordamiento en el Timer2. En esta función, se incrementa la variable `cuenta`, y cuando `cuenta` alcanza un valor de 30 (aproximadamente después de 1 segundo de interrupciones), se cambia el estado del LED incorporado en el pin 13.

## Uso del Código

Este código permite que el LED de la placa Arduino Uno parpadee a una velocidad controlada por interrupciones de tiempo. Puedes utilizarlo como base para aprender cómo funcionan las interrupciones en Arduino y cómo modificar los registros para personalizar el comportamiento del temporizador.