/*
 * Este código hace parpadear el led de la propia placa
 * Arduino Uno cada aproximandamente 1 segundo, haciendo
 * uso de las interrupciones de tiempo.
 * 
 * Para llevar a cabo este tipo de interrupciones no se
 * hace uso de las bibliotecas estandar de Arduino, sino
 * que se modifican los propios registros de las 
 * variables que controlar el Timer2 que tiene Arduino.
 */

volatile unsigned int cuenta = 0; // Contabiliza las veces que se llama a ISR
bool ESTADO = false; // Almacena el estado actual del led

void setup() {
  pinMode(13,OUTPUT);
 
  SREG = (SREG & 0b01111111); // Desabilitar interrupciones
  TIMSK2 = TIMSK2|0b00000001; // Habilita la interrupcion por desbordamiento
  TCCR2B = 0b00000111; // Configura preescala para que FT2 sea de 7812.5Hz
  SREG = (SREG & 0b01111111) | 0b10000000; //Habilitar y desabilitar interrupciones
}


void loop() {
  // Aquí estaría el código del flujo normal de ejecución
}

ISR(TIMER2_OVF_vect) {
  cuenta++; 
  if(cuenta > 29) { // Cada 30 llamadas a ISR cambia el estado del led
    digitalWrite(13,ESTADO);
    ESTADO = !ESTADO;
    cuenta=0;
  }
}
